from typing import Union

import pygame
from miniworldmaker.board_positions import board_position
from miniworldmaker.boards import board as board_module
from miniworldmaker.boards.token_connectors.pixel_board_connector import PixelBoardConnector
from miniworldmaker.tokens import token as token_module
import miniworldmaker


class PixelBoard(miniworldmaker.Board):
    pass
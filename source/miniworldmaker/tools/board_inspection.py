import inspect
import miniworldmaker
from typing import Optional, Union
from miniworldmaker.tokens import token
from miniworldmaker.tools.mwminspection import MWMInspection

class BoardInspection(MWMInspection):
    pass
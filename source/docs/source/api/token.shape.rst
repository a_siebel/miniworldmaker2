Token: Shapes
#############

Shape
======

Base class for shapes


.. autoclass:: miniworldmaker.tokens.shapes.Shape
   :members:

Point
=====

.. autoclass:: miniworldmaker.tokens.shapes.Point
   :members:

Circle
======

.. autoclass:: miniworldmaker.tokens.shapes.Circle
   :members:

Ellipse
=======

.. autoclass:: miniworldmaker.tokens.shapes.Ellipse
   :members:

Line
====

.. autoclass:: miniworldmaker.tokens.shapes.Line
   :members:

Rectangle
=========

.. autoclass:: miniworldmaker.tokens.shapes.Rectangle
   :members:


Polygon
=======

.. autoclass:: miniworldmaker.tokens.shapes.Polygon
   :members:

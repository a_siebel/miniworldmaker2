Token.Texts and Numbers
#######################

TextToken
*********

.. autoclass:: miniworldmaker.tokens.text_token.Text
   :members:
   :noindex:

.. autoclass:: miniworldmaker.tokens.text_token.TextToken
   :members:
   :noindex:


NumberToken
***********

.. autoclass:: miniworldmaker.tokens.number_token.Number
   :members:
   :noindex:

.. autoclass:: miniworldmaker.tokens.number_token.NumberToken
   :members:
   :noindex:
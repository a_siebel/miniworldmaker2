Timer
#####

.. autoclass:: miniworldmaker.tools.timer.Timer
   :members:


ActionTimer
###########

.. autoclass:: miniworldmaker.tools.timer.ActionTimer
   :members:

LoopActionTimer
###############

.. autoclass:: miniworldmaker.tools.timer.LoopActionTimer
   :members:
Objects First
********************************

.. toctree::
    :maxdepth: 1

    01_installation
    02_00_board
    02_01_naming_and_variables
    02_02_imports
    03_tokens
    04_00_dynamic
    04_01_codeblocks
    05_movement
    06_00_events
    06_01.conditions
    07_00_sensors
    07_02_loops
    07_03_functions
    08_costumes
    09_animations
    10_timers
    11_status
    12_physics
    13_creating_miniworlds


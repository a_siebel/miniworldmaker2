# Creating miniworlds

You now have everything you need to create miniworlds:

* With boards and tokens you can design your miniworld.
* With events and sensors you can react to events.

In the repository [miniworldmaker_cookbook](https://codeberg.org/a_siebel/miniworldmaker_cookbook/src/branch/main/)
you will find many examples that you can use as templates.

In the folder ``objects_first`` you will find some examples that were programmed with the *objects-First*, on which
on which this tutorial is based.

## Examples:

### Kara (beginner)

An example of how the Kara programming environment can be translated into miniworldmaker.

<video controls loop width=100%>
  <source src="../_static/kara.webm" type="video/webm">
  Your browser does not support the video tag.
</video>


### Flappy (advanced)

A Flappy Birds example based on a PhysicsBoard.

<video controls loop width=100%>
  <source src="../_static/flappy.webm" type="video/webm">
  Your browser does not support the video tag.
</video>


### RPG (advanced)

An RPG framework based on a TiledBoard. The ``Console`` and ``Toolbar`` extensions
are used to manage equipment and display events.

<video controls loop width=100%>
  <source src="../_static/rpg.webm" type="video/webm">
  Your browser does not support the video tag.
</video>


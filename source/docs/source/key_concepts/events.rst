Events
#######

Keyboard events
***************

on_key_down
===========

.. automethod:: miniworldmaker.tokens.token.Token.on_key_down
   :noindex:


on_key_pressed
===============

.. automethod:: miniworldmaker.tokens.token.Token.on_key_pressed
   :noindex:


    
Mouse events   
*************

on_mouse_left
==============  

.. automethod:: miniworldmaker.tokens.token.Token.on_mouse_left
   :noindex:

on_mouse_right
==============  

.. automethod:: miniworldmaker.tokens.token.Token.on_mouse_right
   :noindex:

on_mouse_motion
===============

.. automethod:: miniworldmaker.tokens.token.Token.on_mouse_motion
   :noindex:

  
on_clicked_left
===============

.. automethod:: miniworldmaker.tokens.token.Token.on_clicked_left
   :noindex:

on_clicked_right
================

.. automethod:: miniworldmaker.tokens.token.Token.on_clicked_right
   :noindex:

Messages
========

.. automethod:: miniworldmaker.tokens.token.Token.on_message
   :noindex:


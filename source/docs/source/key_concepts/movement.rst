Movement
********

Methods
=======

move(distance)
---------------    


.. automethod:: miniworldmaker.tokens.token.Token.move
   :noindex:

move_back()
-----------

.. automethod:: miniworldmaker.tokens.token.Token.move_back
   :noindex:

move_in_direction(direction)
----------------------------

.. automethod:: miniworldmaker.tokens.token.Token.move_in_direction
   :noindex:

move_to(position)
------------------

.. automethod:: miniworldmaker.tokens.token.Token.move_to
   :noindex:
Directions
##########

 
Token direction
---------------

.. autoproperty:: miniworldmaker.tokens.token.Token.direction
   :noindex:

point_in_direction
------------------

Sets direction of token:

.. automethod:: miniworldmaker.tokens.token.Token.point_in_direction
   :noindex:

turn_left
---------

Turns token left by *degrees* degrees

.. automethod:: miniworldmaker.tokens.token.Token.turn_left
   :noindex:

turn_right
----------

Turns token right by *degrees* degrees

.. automethod:: miniworldmaker.tokens.token.Token.turn_right
   :noindex:

flip_x
-------

Mirrors the Token over x-Axis

.. automethod:: miniworldmaker.tokens.token.Token.flip_x
   :noindex:
